import requests
import pandas
import time
import io
from datetime import datetime,date,timedelta
from google.cloud import bigquery
import json
from pandas import json_normalize    
from google.cloud import bigquery


def main(request):
    
    data=pandas.DataFrame() 
    page_size=5000
    offset=0
    finished=False
    while not finished:
        try:
            response = requests.get('https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&resultOffset='+str(offset)+'&&resultRecordCount='+str(page_size)+'&f=json').text
            response_json=json.loads(response)
            print("offset: "+str(offset))
            print("read: "+str(len(response_json['features'])))
            df=json_normalize(map(lambda x: x['attributes'],response_json['features']))
            data=data.append(df)
            offset+=len(response_json['features'])
            if not('exceededTransferLimit' in response_json) or response_json['exceededTransferLimit']==False:
                finished=True
        except Exception as e:
            print("An error occurred at "+str(offset))
            print(e)
            time.sleep(0.3)
            continue

    data['Meldedatum'] = pandas.to_datetime(data['Meldedatum'],unit='ms')
    data['Refdatum'] = pandas.to_datetime(data['Refdatum'],unit='ms')

    print(data.head())
    print(data.dtypes)
    print(data.shape)

    client = bigquery.Client()
    table_id='fwr-263914.rki_covid.cases'
    job_config = bigquery.LoadJobConfig(
        # schema=[
        #     bigquery.SchemaField("budget", bigquery.enums.SqlTypeNames.FLOAT)
        # ],
        write_disposition="WRITE_TRUNCATE"
    )
    job = client.load_table_from_dataframe(
        data,
        table_id,
        job_config=job_config,
        location="europe-west3",  # Must match the destination dataset location.
    )  # Make an API request.
    job.result() 
