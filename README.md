# README #

This script can serve as a google cloud function to download COVID 19 data from the RKI into BigQuery. 

See here for a description of the data source: https://npgeo-corona-npgeo-de.hub.arcgis.com/search?groupIds=b28109b18022405bb965c602b13e1bbc

See this video for explanations: https://www.youtube.com/watch?v=9qJHO4GWorA

This software is provided as is without any guaranties. Please check potential limitations of the data usage at the data source. At the time of publishing the data was freely accessible to the public.
